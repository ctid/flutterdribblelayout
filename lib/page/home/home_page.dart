import 'package:custom_widget_exemple/widget/category_item.dart';
import 'package:custom_widget_exemple/widget/event_item.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          children: [
            //header
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.elliptical(800, 300),
                      bottomLeft: Radius.elliptical(500, 50)),
                  gradient: LinearGradient(
                      begin: Alignment.bottomLeft,
                      end: Alignment.topRight,
                      colors: [
                        Colors.blueAccent.withOpacity(0.9),
                        Colors.blueAccent
                      ]),
                  color: Colors.blueAccent),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  children: [
                    //Title + avatar
                    Padding(
                      padding: const EdgeInsets.fromLTRB(0, 48, 0, 0),
                      child: Row(
                        children: [
                          Text(
                            "Let's plan",
                            style: TextStyle(
                                fontSize: 45,
                                color: Colors.white,
                                fontWeight: FontWeight.w700),
                          ),
                          Spacer(),
                          CircleAvatar(
                              radius: 26,
                              backgroundImage: NetworkImage(
                                  "https://fr.web.img6.acsta.net/pictures/17/02/06/17/01/343859.jpg"))
                        ],
                      ),
                    ),
                    //Subtitle
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0, 48, 0, 24),
                        child: Row(
                          children: [
                            Text(
                              "My schedule",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 24),
                            ),
                            Spacer()
                          ],
                        ),
                      ),
                    ),
                    //categorie items
                    Container(
                      height: 100.0,
                      child: ListView(
                        scrollDirection: Axis.horizontal,
                        children: [
                          CategoryItem(
                              Icon(
                                Icons.airplanemode_active,
                                color: Colors.blue,
                              ),
                              "Vacation",
                              12),
                          CategoryItem(
                              Icon(
                                Icons.shopping_cart,
                                color: Colors.red,
                              ),
                              "Grocery",
                              24),
                          CategoryItem(
                              Icon(
                                Icons.home,
                                color: Colors.purpleAccent,
                              ),
                              "Week-end",
                              3),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            //days
            Container(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(12, 32, 12, 18),
                child: Row(
                  children: [
                    Column(
                      children: [
                        Text(
                          "Today",
                          style: TextStyle(
                            fontSize: 28,
                            color: Colors.blueAccent,
                          ),
                        ),
                        Container(
                          height: 1,
                          width: 70,
                          decoration: BoxDecoration(color: Colors.blueAccent),
                        )
                      ],
                    ),
                    Container(width: 32),
                    Text(
                      "Week",
                      style: TextStyle(
                        fontSize: 28,
                        color: Colors.grey,
                      ),
                    ),
                    Container(width: 32),
                    Text(
                      "Month",
                      style: TextStyle(
                        fontSize: 28,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            //Events
            Expanded(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: ListView(
                  children: [
                    EventItem(Colors.green, "To pet Richard", "10:30 AM"),
                    EventItem(Colors.green, "Water the flower", "12:00 AM"),
                    EventItem(Colors.red, "Branch with Alice", "2:00 PM"),
                    EventItem(Colors.yellow, "Go to the exhibition", "5:00 PM"),
                    Padding(
                      padding: const EdgeInsets.all(48.0),
                      child: FloatingActionButton(
                        onPressed: () => {},
                        child: Icon(Icons.add, size: 32),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
