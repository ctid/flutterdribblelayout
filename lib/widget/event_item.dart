import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EventItem extends StatelessWidget {
  final Color _color;
  final String _title;
  final String _time;

  EventItem(this._color, this._title, this._time);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 180.0,
        child: Card(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(18.0, 24.0, 18.0, 24.0),
            child: Row(
              children: [
                Icon(
                  Icons.circle,
                  color: _color,
                  size: 14,
                ),
                Container(width: 12),
                Text(_title,
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        fontSize: 18.0,
                        color: Colors.black87)),
                Spacer(),
                Text(_time,
                    style: TextStyle(fontSize: 14.0, color: Colors.grey)),
                Container(width: 12),
                Icon(Icons.chevron_right, color: Colors.grey),
              ],
            ),
          ),
        ));
  }
}
