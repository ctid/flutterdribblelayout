import 'package:flutter/material.dart';

class CustomRoundedButton extends StatelessWidget {
  final String _title;
  final IconData _iconData;

  CustomRoundedButton(this._title, this._iconData);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 48.0,
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              colors: [
                Colors.blue,
                Colors.blue.withOpacity(0.8),
              ]),
          color: Colors.blue,
          borderRadius: BorderRadius.all(Radius.circular(12)),
        ),
        child: Row(
          children: [
            Container(width: 16.0),
            Icon(this._iconData, color: Colors.white),
            Spacer(),
            Text(
              this._title,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16.0,
                  fontWeight: FontWeight.w600),
            ),
            Spacer(),
          ],
        ),
      ),
    );
  }
}
