import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CategoryItem extends StatelessWidget {
  final Icon _icon;
  final String _title;
  final int _itemNumber;

  CategoryItem(this._icon, this._title, this._itemNumber);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 160.0,
        child: Card(
          margin: EdgeInsets.all(12.0),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                children: [
                  Container(width: 12),
                  _icon,
                  Container(width: 12),
                ],
              ),
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Spacer(),
                    Text(
                      _title,
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                    ),
                    Text(
                      "$_itemNumber items",
                      style: TextStyle(fontSize: 12, color: Colors.grey),
                    ),
                    Spacer(),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
